'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Gather Schema
 */
var GatherSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Gather name',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Gather', GatherSchema);
