'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Linguist Schema
 */
var LinguistSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Linguist name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Linguist', LinguistSchema);