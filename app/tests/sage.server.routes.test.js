'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Sage = mongoose.model('Sage'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, sage;

/**
 * Sage routes tests
 */
describe('Sage CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Sage
		user.save(function() {
			sage = {
				name: 'Sage Name'
			};

			done();
		});
	});

	it('should be able to save Sage instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Sage
				agent.post('/sage')
					.send(sage)
					.expect(200)
					.end(function(sageSaveErr, sageSaveRes) {
						// Handle Sage save error
						if (sageSaveErr) done(sageSaveErr);

						// Get a list of Sages
						agent.get('/sage')
							.end(function(sagesGetErr, sagesGetRes) {
								// Handle Sage save error
								if (sagesGetErr) done(sagesGetErr);

								// Get Sages list
								var sages = sagesGetRes.body;

								// Set assertions
								(sages[0].user._id).should.equal(userId);
								(sages[0].name).should.match('Sage Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Sage instance if not logged in', function(done) {
		agent.post('/sage')
			.send(sage)
			.expect(401)
			.end(function(sageSaveErr, sageSaveRes) {
				// Call the assertion callback
				done(sageSaveErr);
			});
	});

	it('should not be able to save Sage instance if no name is provided', function(done) {
		// Invalidate name field
		sage.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Sage
				agent.post('/sage')
					.send(sage)
					.expect(400)
					.end(function(sageSaveErr, sageSaveRes) {
						// Set message assertion
						(sageSaveRes.body.message).should.match('Please fill Sage name');
						
						// Handle Sage save error
						done(sageSaveErr);
					});
			});
	});

	it('should be able to update Sage instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Sage
				agent.post('/sage')
					.send(sage)
					.expect(200)
					.end(function(sageSaveErr, sageSaveRes) {
						// Handle Sage save error
						if (sageSaveErr) done(sageSaveErr);

						// Update Sage name
						sage.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Sage
						agent.put('/sage/' + sageSaveRes.body._id)
							.send(sage)
							.expect(200)
							.end(function(sageUpdateErr, sageUpdateRes) {
								// Handle Sage update error
								if (sageUpdateErr) done(sageUpdateErr);

								// Set assertions
								(sageUpdateRes.body._id).should.equal(sageSaveRes.body._id);
								(sageUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Sages if not signed in', function(done) {
		// Create new Sage model instance
		var sageObj = new Sage(sage);

		// Save the Sage
		sageObj.save(function() {
			// Request Sages
			request(app).get('/sage')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Sage if not signed in', function(done) {
		// Create new Sage model instance
		var sageObj = new Sage(sage);

		// Save the Sage
		sageObj.save(function() {
			request(app).get('/sage/' + sageObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', sage.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Sage instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Sage
				agent.post('/sage')
					.send(sage)
					.expect(200)
					.end(function(sageSaveErr, sageSaveRes) {
						// Handle Sage save error
						if (sageSaveErr) done(sageSaveErr);

						// Delete existing Sage
						agent.delete('/sage/' + sageSaveRes.body._id)
							.send(sage)
							.expect(200)
							.end(function(sageDeleteErr, sageDeleteRes) {
								// Handle Sage error error
								if (sageDeleteErr) done(sageDeleteErr);

								// Set assertions
								(sageDeleteRes.body._id).should.equal(sageSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Sage instance if not signed in', function(done) {
		// Set Sage user 
		sage.user = user;

		// Create new Sage model instance
		var sageObj = new Sage(sage);

		// Save the Sage
		sageObj.save(function() {
			// Try deleting Sage
			request(app).delete('/sage/' + sageObj._id)
			.expect(401)
			.end(function(sageDeleteErr, sageDeleteRes) {
				// Set message assertion
				(sageDeleteRes.body.message).should.match('User is not logged in');

				// Handle Sage error error
				done(sageDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Sage.remove().exec();
		done();
	});
});
