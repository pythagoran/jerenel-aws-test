'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Linguist = mongoose.model('Linguist'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, linguist;

/**
 * Linguist routes tests
 */
describe('Linguist CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Linguist
		user.save(function() {
			linguist = {
				name: 'Linguist Name'
			};

			done();
		});
	});

	it('should be able to save Linguist instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Linguist
				agent.post('/linguist')
					.send(linguist)
					.expect(200)
					.end(function(linguistSaveErr, linguistSaveRes) {
						// Handle Linguist save error
						if (linguistSaveErr) done(linguistSaveErr);

						// Get a list of Linguists
						agent.get('/linguist')
							.end(function(linguistsGetErr, linguistsGetRes) {
								// Handle Linguist save error
								if (linguistsGetErr) done(linguistsGetErr);

								// Get Linguists list
								var linguists = linguistsGetRes.body;

								// Set assertions
								(linguists[0].user._id).should.equal(userId);
								(linguists[0].name).should.match('Linguist Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Linguist instance if not logged in', function(done) {
		agent.post('/linguist')
			.send(linguist)
			.expect(401)
			.end(function(linguistSaveErr, linguistSaveRes) {
				// Call the assertion callback
				done(linguistSaveErr);
			});
	});

	it('should not be able to save Linguist instance if no name is provided', function(done) {
		// Invalidate name field
		linguist.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Linguist
				agent.post('/linguist')
					.send(linguist)
					.expect(400)
					.end(function(linguistSaveErr, linguistSaveRes) {
						// Set message assertion
						(linguistSaveRes.body.message).should.match('Please fill Linguist name');
						
						// Handle Linguist save error
						done(linguistSaveErr);
					});
			});
	});

	it('should be able to update Linguist instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Linguist
				agent.post('/linguist')
					.send(linguist)
					.expect(200)
					.end(function(linguistSaveErr, linguistSaveRes) {
						// Handle Linguist save error
						if (linguistSaveErr) done(linguistSaveErr);

						// Update Linguist name
						linguist.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Linguist
						agent.put('/linguist/' + linguistSaveRes.body._id)
							.send(linguist)
							.expect(200)
							.end(function(linguistUpdateErr, linguistUpdateRes) {
								// Handle Linguist update error
								if (linguistUpdateErr) done(linguistUpdateErr);

								// Set assertions
								(linguistUpdateRes.body._id).should.equal(linguistSaveRes.body._id);
								(linguistUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Linguists if not signed in', function(done) {
		// Create new Linguist model instance
		var linguistObj = new Linguist(linguist);

		// Save the Linguist
		linguistObj.save(function() {
			// Request Linguists
			request(app).get('/linguist')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Linguist if not signed in', function(done) {
		// Create new Linguist model instance
		var linguistObj = new Linguist(linguist);

		// Save the Linguist
		linguistObj.save(function() {
			request(app).get('/linguist/' + linguistObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', linguist.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Linguist instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Linguist
				agent.post('/linguist')
					.send(linguist)
					.expect(200)
					.end(function(linguistSaveErr, linguistSaveRes) {
						// Handle Linguist save error
						if (linguistSaveErr) done(linguistSaveErr);

						// Delete existing Linguist
						agent.delete('/linguist/' + linguistSaveRes.body._id)
							.send(linguist)
							.expect(200)
							.end(function(linguistDeleteErr, linguistDeleteRes) {
								// Handle Linguist error error
								if (linguistDeleteErr) done(linguistDeleteErr);

								// Set assertions
								(linguistDeleteRes.body._id).should.equal(linguistSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Linguist instance if not signed in', function(done) {
		// Set Linguist user 
		linguist.user = user;

		// Create new Linguist model instance
		var linguistObj = new Linguist(linguist);

		// Save the Linguist
		linguistObj.save(function() {
			// Try deleting Linguist
			request(app).delete('/linguist/' + linguistObj._id)
			.expect(401)
			.end(function(linguistDeleteErr, linguistDeleteRes) {
				// Set message assertion
				(linguistDeleteRes.body.message).should.match('User is not logged in');

				// Handle Linguist error error
				done(linguistDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Linguist.remove().exec();
		done();
	});
});
