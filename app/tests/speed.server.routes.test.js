'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Speed = mongoose.model('Speed'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, speed;

/**
 * Speed routes tests
 */
describe('Speed CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Speed
		user.save(function() {
			speed = {
				name: 'Speed Name'
			};

			done();
		});
	});

	it('should be able to save Speed instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speed
				agent.post('/speeds')
					.send(speed)
					.expect(200)
					.end(function(speedSaveErr, speedSaveRes) {
						// Handle Speed save error
						if (speedSaveErr) done(speedSaveErr);

						// Get a list of Speeds
						agent.get('/speeds')
							.end(function(speedsGetErr, speedsGetRes) {
								// Handle Speed save error
								if (speedsGetErr) done(speedsGetErr);

								// Get Speeds list
								var speeds = speedsGetRes.body;

								// Set assertions
								(speeds[0].user._id).should.equal(userId);
								(speeds[0].name).should.match('Speed Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Speed instance if not logged in', function(done) {
		agent.post('/speeds')
			.send(speed)
			.expect(401)
			.end(function(speedSaveErr, speedSaveRes) {
				// Call the assertion callback
				done(speedSaveErr);
			});
	});

	it('should not be able to save Speed instance if no name is provided', function(done) {
		// Invalidate name field
		speed.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speed
				agent.post('/speeds')
					.send(speed)
					.expect(400)
					.end(function(speedSaveErr, speedSaveRes) {
						// Set message assertion
						(speedSaveRes.body.message).should.match('Please fill Speed name');
						
						// Handle Speed save error
						done(speedSaveErr);
					});
			});
	});

	it('should be able to update Speed instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speed
				agent.post('/speeds')
					.send(speed)
					.expect(200)
					.end(function(speedSaveErr, speedSaveRes) {
						// Handle Speed save error
						if (speedSaveErr) done(speedSaveErr);

						// Update Speed name
						speed.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Speed
						agent.put('/speeds/' + speedSaveRes.body._id)
							.send(speed)
							.expect(200)
							.end(function(speedUpdateErr, speedUpdateRes) {
								// Handle Speed update error
								if (speedUpdateErr) done(speedUpdateErr);

								// Set assertions
								(speedUpdateRes.body._id).should.equal(speedSaveRes.body._id);
								(speedUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Speeds if not signed in', function(done) {
		// Create new Speed model instance
		var speedObj = new Speed(speed);

		// Save the Speed
		speedObj.save(function() {
			// Request Speeds
			request(app).get('/speeds')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Speed if not signed in', function(done) {
		// Create new Speed model instance
		var speedObj = new Speed(speed);

		// Save the Speed
		speedObj.save(function() {
			request(app).get('/speeds/' + speedObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', speed.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Speed instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Speed
				agent.post('/speeds')
					.send(speed)
					.expect(200)
					.end(function(speedSaveErr, speedSaveRes) {
						// Handle Speed save error
						if (speedSaveErr) done(speedSaveErr);

						// Delete existing Speed
						agent.delete('/speeds/' + speedSaveRes.body._id)
							.send(speed)
							.expect(200)
							.end(function(speedDeleteErr, speedDeleteRes) {
								// Handle Speed error error
								if (speedDeleteErr) done(speedDeleteErr);

								// Set assertions
								(speedDeleteRes.body._id).should.equal(speedSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Speed instance if not signed in', function(done) {
		// Set Speed user 
		speed.user = user;

		// Create new Speed model instance
		var speedObj = new Speed(speed);

		// Save the Speed
		speedObj.save(function() {
			// Try deleting Speed
			request(app).delete('/speeds/' + speedObj._id)
			.expect(401)
			.end(function(speedDeleteErr, speedDeleteRes) {
				// Set message assertion
				(speedDeleteRes.body.message).should.match('User is not logged in');

				// Handle Speed error error
				done(speedDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Speed.remove().exec();
		done();
	});
});