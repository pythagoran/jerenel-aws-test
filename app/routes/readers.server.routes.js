'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var readers = require('../../app/controllers/readers.server.controller');

	// Readers Routes
	app.route('/api/readers')
		.get(readers.speak);
};
