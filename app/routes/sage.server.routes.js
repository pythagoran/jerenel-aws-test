'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var sages = require('../controllers/sage.server.controller.js');

	// Sages Routes
	app.route('/api/sage')
		.get(sages.list);
};
