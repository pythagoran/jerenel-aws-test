'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var linguists = require('../../app/controllers/linguists.server.controller');

	// Linguist Routes
	app.route('/api/linguist')
		.get(linguists.analyze);
};
