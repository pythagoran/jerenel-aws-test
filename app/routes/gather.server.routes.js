'use strict';

module.exports = function(app) {

    var gather = require('../../app/controllers/gather.server.controller');

    app.route('/api/gather')
        .get(gather.scrape);

};
