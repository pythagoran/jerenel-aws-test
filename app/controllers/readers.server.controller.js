'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	_ = require('lodash'),
    mkdirp = require('mkdirp'),
    fs = require('fs'),
    exec = require('child_process').exec,
    child;

/**
 * Create a Reader
 */
exports.speak = function(req, res) {
    var dir = './public/data/audio/' + new Date().toISOString().slice(0, 10),
        filename = new Date().valueOf() + '.txt',
        filepath = dir + '/' + filename;

    mkdirp(dir, function (err) {
        if (err) res.send(err);
        else makeTextFile();
    });

    function makeTextFile() {
        var opts = {encoding:'ascii'};
        // var splitText = req.query.data.match(/\n|([^\r\n.!?]+([.!?]+|$))/gim);
        // 1. Create new file for every sentence, parentheses, dashed asides, etc.
        // 2. Add break for each newline (paragraph)
        fs.writeFile(filepath, req.query.data, opts, function(err) {
            if (err) res.send(err);
            else makeWave();
        });
    }

    function makeWave() {
        // Convert multiple files
        // Track progress
        text2wave(dir, filename, function(result) {
            if (result.length === 2) {
                var fileDir = result[0].slice(9), // <- ./public/data/audio/.../...
                    execTime = result[1];
                res.send({file:fileDir, time:execTime});
            }
            else res.send(result);
        });
    }

    function text2wave(dir, filename, callback) {
        var filewav = filename.replace('.txt', '.wav'),
            command = 'text2wave ' + filename + ' -o ' + filewav,
            opts = {cwd: dir},
            then = new Date().valueOf(),
            now;

        child = exec(command, opts, function (error, stdout, stderr) {
            now = new Date().valueOf();
            if (error !== null) callback([error]);
            else callback([dir + '/' + filewav, now-then]);
        });
    }
};
