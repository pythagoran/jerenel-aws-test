'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Sage = mongoose.model('Sage'),
	_ = require('lodash');

/**
 * List of Sages
 */
exports.list = function(req, res) { 
	Sage.find().sort('-created').populate('user', 'displayName').exec(function(err, sages) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(sages);
		}
	});
};
