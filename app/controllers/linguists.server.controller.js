'use strict';

/* jshint evil:true */

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Linguist = mongoose.model('Linguist'),
	_ = require('lodash'),
    salient = require('salient'),
    lexrank = require('lexrank'),
    textrank = require('textrank-js'),
    unfluff = require('unfluff'),
    natural = require('natural'),
    topia = require('topia'),
    treetagger = require('treetagger');


//var tt = new salient.tagging.TreeTagger();
//tt.tag('Hello, World!', function(err, result) {
//    console.log(result);
//});
//

/**
 * Create a Linguist
 */
exports.analyze = function(req, res) {
    if (req.query.hasOwnProperty('nlpData')) {
        new treetagger().tag(req.query.nlpData, function (err, results) {
            res.send({data:results});
        });
    }
    if (req.query.hasOwnProperty('nlpText')) {
        var sentiment = new salient.sentiment.BayesSentimentAnalyser();
        var response = [];
        _.forEach(req.query.nlpText, function(val) {
            response.push(sentiment.classify(val));
        });
        res.send({data:response});
    }
    if (req.query.hasOwnProperty('summary')) {
        var summary = req.query.summary;
        //var lines = Math.round(summary.data.split('. ').length);
        lexrank.summarize(summary[0], summary[1], function(err, toplines, text) {
            res.send({toplines: toplines, text: text});
        });
    }
    if (req.query.hasOwnProperty('textrank')) {
        // First convert to a keyword graph as described in the paper
        var tokenize = req.query.textrank;
        var tt = new treetagger();
        tt.tag(tokenize, function (err, results) {
            results.map(function (item) {
                item.term = item.t;
                delete item.l;
                delete item.t;
            });
            var graph = textrank.keyExGraph(results);
            // Now run text rank on the graph
            var ws = textrank.textRank(graph);
            // Get the top N keywords
            ws = ws.slice(0, Math.min(ws.length, 10));
            var rankedTerms = [];
            ws.forEach(function (item) {
                rankedTerms.push(item.name + ' --> ' + item.score);
            });
            res.send({ result: rankedTerms });
        });
    }
    if (req.query.hasOwnProperty('keyTerms')) {
        var TfIdf = natural.TfIdf,
            tfidf = new TfIdf(),
            terms = [];

        tfidf.addDocument(req.query.keyTerms);

        tfidf.listTerms(0 /*document index*/).forEach(function(item) {
            terms.push([item.term, item.tfidf]);
        });
        res.send({ data: terms });
        //var tagger = new topia.Tagger({},{
        //    'lexicon' : topia.lexicon
        //});
        //var filter = new topia.DefaultFilter();
        //var extractor = new topia.TermExtraction({
        //    'tagger' : tagger, 'filter' : filter
        //});
        //
        //var text = req.query.keyTerms;
        //var tags = extractor.call(text);
        ////tags.sort();
        //res.send({ data: tags });
    }
};
