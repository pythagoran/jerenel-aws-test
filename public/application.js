'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.html5Mode(ApplicationConfiguration.html5Mode).hashPrefix('!');
	}
]);

angular.module(ApplicationConfiguration.applicationModuleName).run(function($rootScope, $location, $state, Authentication) {
    $rootScope.$on( '$stateChangeStart', function(e, toState  , toParams, fromState, fromParams) {
        $rootScope.authentication = Authentication;

        var isLogin = function() { return "signup signin forgot reset-invalid reset-success reset".indexOf(toState.name) > -1};
        if (isLogin()) return;

        if(!$rootScope.authentication.user.hasOwnProperty('_id')) {
            e.preventDefault();
            $state.go('signin');
        }
    });
});

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
