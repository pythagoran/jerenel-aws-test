'use strict';

// Configuring the Linguist module
angular.module('linguist').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Linguist', 'linguist', 'item', '/linguist', false);
	}
]);
