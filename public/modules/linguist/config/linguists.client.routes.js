'use strict';

// Setting up route
angular.module('linguist').config(['$stateProvider',
	function($stateProvider) {
		// Linguists state routing
		$stateProvider.
		state('linguist', {
			url: '/linguist',
			templateUrl: 'modules/linguist/views/linguist.client.view.html'
		});
	}
]);
