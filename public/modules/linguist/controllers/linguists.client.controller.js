'use strict';

// Linguist controller
angular.module('linguist').controller('LinguistController', ['$scope', '$stateParams', '$location', 'Linguist', 'nlp',
	function($scope, $stateParams, $location, Linguist, nlp) {
        $scope.todos = [
            {val:'Syntactic trees'},
            {val:'Parts of speech extraction'},
            {val:'Keyword extraction'},
            {val:'Relevant block identification'},
            {val:'Large-text summaries'},
            {val:'Pronunciation visualization'}
        ];

        /*
		 * Natural Language Processing
         */

        $scope.linguist = {
            analyze: function(text, cb) {
                if (!text) {
                    Linguist.request.get({ nlpData: vm.text }, function(data) {
                        console.log(data);
                        $scope.linguist.result.sentences = [];
                        var sent = [],
                            poses = [];
                        angular.forEach(data.data, function(pos) {
                            if (pos.pos !== 'SENT') {
                                sent.push(pos.t);
                                poses.push(pos.pos);
                            } else {
                                sent.push(pos.t);
                                poses.push(pos.pos);
                                $scope.linguist.result.sentences.push([sent, poses]);
                                sent = []; poses = [];
                            }
                        });
                    });
                } else {
                    Linguist.request.get({ nlpText:text }, function(data) {
                        cb(data.data);
                    });
                }
            },
            result: {
                sentences: []
            }
        };

        var vm = $scope.vm = {};
        vm.text = 'An orange surge that has vaulted the federal NDP from third place to a statistical dead-heat with Conservatives and Liberals is the real deal, new numbers suggest.\n\nA poll from EKOS, released for iPolitics Friday, has Thomas Mulcair\'s party on top with 29.6 per cent support, followed by the Tories at 28.1 per cent, and Liberals at 26.1 per cent. Greens are at 7.6 per cent.\n\nEKOS president Frank Graves has suggested the results should put to rest any doubts about NDP momentum since a poll from his firm last week first pointed to a three-way tie.\n\n"We can ask how long it might last, but no one can dismiss these numbers as the result of a rogue poll or sampling error," he wrote in iPolitics. "It\'s real — get over it."';
        vm.nouns = null;
        vm.adjectives = null;
        vm.adverbs = null;
        vm.verbs = null;
        vm.values = null;

        $scope.$watch('vm.text', function(text) {
            if (!text) { vm.nouns = null; vm.adjectives = null; vm.adverbs = null; vm.verbs = null; vm.values = null; return; }

            var pos = nlp.pos(text);
            var lin = $scope.linguist;

            vm.nouns = pos.nouns().map(function(ele) {
                return ele.normalised;
            });
            vm.nouns = _.uniq(vm.nouns);
            vm.nouns = vm.nouns.join(', ');

            vm.adjectives = pos.adjectives().map(function(ele) {
                return ele.normalised;
            });
            vm.adjectives = _.uniq(vm.adjectives);
            vm.adjectives = vm.adjectives.join(', ');

            vm.adverbs = pos.adverbs().map(function(ele) {
                return ele.normalised;
            });
            vm.adverbs = _.uniq(vm.adverbs);
            vm.adverbs = vm.adverbs.join(', ');

            vm.verbs = pos.verbs().map(function(ele) {
                return ele.normalised;
            });
            vm.verbs = _.uniq(vm.verbs);
            vm.verbs = vm.verbs.join(', ');

            vm.values = pos.values().map(function(ele) {
                return ele.normalised;
            });
            vm.values = _.uniq(vm.values);
            vm.values = vm.values.join(', ');

            var sentimentText = nlp.sentences(text);
            lin.analyze(sentimentText, function(data) {
                vm.sentiments = data;
                vm.sentiments = vm.sentiments.join(', ');
            });

            $scope.summary.summarize();
        });

        /*
         * LexRank
         */

        $scope.summary= {
            text: 'Bank of Montreal\'s chief economist believes the angst over the real estate market is linked to a persistent gap between U.S. and Canadian house prices.\n\nDouglas Porter tracked the two markets, finding that prices in both countries climbed about 5 per cent over the past year.\n\n"This unusual period of symmetry follows a wild 10-year period which saw the U.S. market boom, then collapse, then finally recover strongly for a spell," the BMO economist said in a research note.\n\nIn contrast, but for the "wild swing" during and quickly following the financial crisis, Canadian home prices have been "chugging along," he found.\n\n"What makes this recent period of relative calm between the two national markets so notable is that Canadian prices took about a 60-per-cent step ahead of U.S. prices in a six-year span from 2006-2012," Mr. Porter said, noting that prices "largely tracked each other" until 2006, as the chart above shows. .\n\n"Then, after a moderate step back, they have largely stayed there, far above their U.S. counterparts," he added.\n\n"It\'s this gap that - to this day - has so many calling for a deep correction in Canada."\n\nWhile many observers have issued warnings about Canada\'s housing market, Bay Street economists don\'t see a meltdown coming, though some predict a natural cooling.\n\nNor does the Bank of Canada, though it is keeping a wary eye on prices, believing them to be inflated by between 10 and 30 per cent.\n\nMuch of the angst, of course, has to do with the booming Toronto and Vancouver markets.\n\n"Strong demand from international migrants and young millennials, an influx of foreign wealth, and low mortgage rates are driving the two markets," Mr. Porter\'s colleague at BMO, senior economist Sal Guatieri, said just this week in a separate economic forecast, citing a price surge in single-family abodes in those cities.',
            lines: 5,
            resultLexRank: undefined,
            resultTextRank: undefined,
            summarize: function(cb) {
                var sentences,
                    text = [];
                sentences = nlp.sentences(this.text);
                sentences.map(function(sentence) {
                    var tempSentence = [];
                    nlp.tokenize(sentence)[0].tokens.map(function(token) {
                        tempSentence.push(token.normalised);
                    });
                    if (tempSentence.length>1) text.push(tempSentence);
                });
                //console.log(text);
                Linguist.request.get({textrank: this.text}, function(data) {
                //Linguist.request.get({textrank: text}, function(data) {
                    //$scope.summary.resultTextRank = data;
                    //data.tt.map(function(item) {
                    console.log(data);
                    //})
                });
                var s = [this.text, this.lines];
                //Linguist.request.get({ summary: s }, function(data) {
                //    $scope.summary.resultLexRank = data;
                //    console.log(data);
                //})
                Linguist.summarize({ text: this.text, lines: this.lines }, function(data) {
                    //console.log(data);
                    $scope.summary.resultLexRank = data;
                });

                // Temporary hack...
                String.prototype.count = function(search) {
                    var m = this.match(new RegExp(search.toString().replace(/(?=[.\\+*?[^\]$(){}\|])/g, '\\'), 'g'));
                    return m ? m.length:0;
                };

                // TODO: Add stopwords for ranking
                Linguist.request.get({ keyTerms: this.text }, function(data) {
                    setTimeout(function() {
                        data.data.map(function(term) {
                            var count = $scope.summary.resultLexRank.text.count(term[0]);
                            if (count > 1) {
                                console.log(term[0] + ': ' + term[1] + ' | count: ' + count);
                            }
                        });
                    }, 1000);
                });
            }
        };


	}
]);
