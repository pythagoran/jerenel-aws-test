'use strict';

/* jshint ignore:start*/

// Linguist service used to communicate Linguist REST endpoints
angular.module('linguist').factory('Linguist', ['$resource',
	function($resource) {

        function lexrank(sentences, params) {

            // set default parameters
            //if (params == null) params = {};
            if (params.pagerank == null) params.pagerank = {}; // see pagerank.js
            if (params.threshold == null) params.threshold = .1;
            if (params.idf == null) params.idf = {};
            if (params.sort_by_score == null) params.sort_by_score = true; // result

            // sentence -> tf vector  // need reveiew
            var word2id = {}; var id2word = [];
            var word_id = 0;
            //var tf_vecs = [];
            var tfidf_vecs = [];

            var tf2tfidf = function(tf) {
                var tfidf = [];
                for (var wid in tf) {
                    tfidf[wid] = tf[wid] * ((params.idf[id2word[wid]] != null)
                        ? params.idf[id2word[wid]] : 1.0);
                }
                return tfidf;
            };

            sentences.forEach(function(s) {
                var tf = [];
                params.word_segmenter(s).forEach(function(w) {
                    if (word2id[w] == null) {
                        word2id[w] = word_id;
                        id2word[word_id] = w;
                        word_id++;
                    }
                    tf[word2id[w]] = (tf[word2id[w]] == null) ? 1 : (tf[word2id[w]] + 1);
                });
                //tf_vecs.push(tf);
                tfidf_vecs.push(tf2tfidf(tf));
            });
            //console.log(tf_vecs.length);
            //console.log(word2id);
            //console.log(id2word);

            // compute similarities between sentences and prepare a graph.

            // tools
            var values = function(obj) {
                var xs = [];
                for (var key in obj) xs.push(obj[key]);
                return xs;
            };

            var sum = function(xs) {
                return xs.reduce(function(prev, current, idx, all) {
                    return prev + current;
                });
            };

            var norm = function(xs) {
                return Math.sqrt(sum(xs.map(function(v, i, all) { return v * v; })));
            };

            var cos_sim = function(tf1, tf2) { // tf or tf*idf
                if (Object.keys(tf1).length == 0 || Object.keys(tf2).length == 0)
                    return 0;
                var a = 0;
                Object.keys(tf1).forEach(function(w) {
                    a += tf1[w] * ((tf2[w] != null) ? tf2[w] : 0);
                });
                var b = norm(values(tf1)) * norm(values(tf2));
                return a / b;
            };

            //var similarities = [];
            var graph = {};
            for (var i = 0; i < sentences.length; i++) {
                graph[i] = [];
                //similarities[i] = [];
            }

            for (var i = 0; i < sentences.length; i++) {
                for (var j = i + 1; j < sentences.length; j++) {
                    var score = cos_sim(tfidf_vecs[i], tfidf_vecs[j]);
                    //similarities[i][j] = score;
                    if (score >= params.threshold) {
                        graph[i].push(j);
                        graph[j].push(i);
                    }
                }
            }
            //console.log(graph);

            // apply pagerank
            var score = pagerank(graph, params.pagerank);
            //console.log(score);

            var sents_meta = [];
            for (var i = 0; i < sentences.length; i++) {
                //sents_meta.push({idx: i, score: score[i], sims: similarities[i]});
                sents_meta.push({idx: i, score: score[i]});
            }

            if (params.sort_by_score == true) {
                sents_meta.sort(function(a, b) {
                    return b.score - a.score;
                });
            }

            return sents_meta;
        }

        // based on networkx.pagerank 1.9.1 (Python)
        // https://github.com/networkx/networkx/blob/master/networkx/algorithms/link_analysis/pagerank_alg.py
        function pagerank(G, params) {

            // set default parameters
            if (params == null) params = {};
            if (params.alpha == null) params.alpha = 0.85;
            if (params.tol == null) params.tol = 1.0e-6;
            if (params.max_iter == null) params.max_iter = 100;

            // tools
            var sum = function(xs) {
                return xs.reduce(function(prev, current, idx, xs) {
                    return prev + current;
                });
            };

            var N = Object.keys(G).length
            if (N == 0) return {};

            // Create a copy in (right) stochastic form
            var W = {}
            for (var node in G) {
                var nodes = G[node];
                var num_nodes = nodes.length;
                var new_nodes = {};
                //for (var node_ in nodes) {
                nodes.forEach(function(node_) {
                    new_nodes[node_] = {'weight': 1.0 / num_nodes};
                });
                W[node] = new_nodes;
            }
            //console.log(W);

            // Choose fixed starting vector if not given
            var x = {};
            if (params.nstart == null) {
                for (var node in W) x[node] = 1.0 / N;
            }
            else {
                // Normalized nstart vector
                var sum_ = sum(Object.values(params.nstart));
                for (var node in params.nstart)
                    x[node] = params.nstart[node] / sum_;
            }
            //console.log(x);

            var p = {};
            if (params.personalization == null) {
                // Assign uniform personalization vector if not given
                for (var node in W) p[node] = 1.0 / N;
            }
            else {
                // TODO: check missing nodes
                var sum_ = sum(Object.values(params.personalization));
                for (var node in params.personalization)
                    p[node] = params.personalization[node] / sum_;
            }

            var dangling_weights = {};
            if (params.dangling == null) {
                // Use personalization vector if dangling vector not specified
                dangling_weights = p;
            }
            else {
                // TODO: check missing nodes
                var sum_ = sum(Object.values(params.dangling));
                for (var node in params.dangling)
                    dangling_weights[node] = params.dangling[node] / sum_;
            }
            var dangling_nodes = [];
            for (var node in W) {
                if (Object.keys(W[node]).length == 0) // need review
                    dangling_nodes.push(node);
            }
            //console.log(dangling_nodes);

            // power iteration: make up to max_iter iterations
            for (var iter_count = 0; iter_count < params.max_iter; iter_count++) {
                var xlast = x;
                x = {};
                for (var node in xlast) x[node] = 0.0;
                var sum_ = 0.0;
                dangling_nodes.forEach(function(node) {
                    sum_ += xlast[node]
                });
                var danglesum = params.alpha * sum_;

                for (var node in x) {
                    // this matrix multiply looks odd because it is
                    // doing a left multiply x^T=xlast^T*W
                    for (var nbr in W[node]) {
                        x[nbr] += params.alpha * xlast[node] * W[node][nbr]['weight'];
                    }
                    x[node] += danglesum * dangling_weights[node] + (1.0 - params.alpha) * p[node];
                }

                // check convergence, l1 norm
                var err = 0.0;
                Object.keys(x).forEach(function(node) {
                    err += Math.abs(x[node] - xlast[node])
                });
                if (err < N*params.tol)
                    return x;
            }

            console.warn('pagerank: power iteration failed to converge in ' +
            params.iter_max + ' iterations.');
            return x;
        }
        // test
        //var scores = pagerank({1: [2, 3, 4], 2: [3], 3: [4], 4: []}, {alpha: 0.9});
        //console.log(scores);
        //
        //var scores = pagerank({'foo': ['bar', 'hoge'], 'bar': [], 'hoge': ['foo']});
        //console.log(scores);

        return {
            request: $resource('api/linguist'),
            summarize: function(data, cb) {
                var text = data.text;
                var num_sent = data.lines;
                //console.log(text);
                //console.log(num_sent);

                var sent_splitter = function(text) {
                    return text.replace(/([^.!?A-Z]+[.?!]+\s*)/g, '$1|').split('|');
                };

                // https://sites.google.com/site/kevinbouge/stopwords-lists
                var _stopwords = 'a a\'s able about above according accordingly across actually after afterwards again against ain\'t all allow allows almost alone along already also although always am among amongst an and another any anybody anyhow anyone anything anyway anyways anywhere apart appear appreciate appropriate are aren\'t around as aside ask asking associated at available away awfully b be became because become becomes becoming been before beforehand behind being believe below beside besides best better between beyond both brief but by c c\'mon c\'s came can can\'t cannot cant cause causes certain certainly changes clearly co com come comes concerning consequently consider considering contain containing contains corresponding could couldn\'t course currently d definitely described despite did didn\'t different do does doesn\'t doing don\'t done down downwards during e each edu eg eight either else elsewhere enough entirely especially et etc even ever every everybody everyone everything everywhere ex exactly example except f far few fifth first five followed following follows for former formerly forth four from further furthermore g get gets getting given gives go goes going gone got gotten greetings h had hadn\'t happens hardly has hasn\'t have haven\'t having he he\'s hello help hence her here here\'s hereafter hereby herein hereupon hers herself hi him himself his hither hopefully how howbeit however i i\'d i\'ll i\'m i\'ve ie if ignored immediate in inasmuch inc indeed indicate indicated indicates inner insofar instead into inward is isn\'t it it\'d it\'ll it\'s its itself j just k keep keeps kept know knows known l last lately later latter latterly least less lest let let\'s like liked likely little look looking looks ltd m mainly many may maybe me mean meanwhile merely might more moreover most mostly much must my myself n name namely nd near nearly necessary need needs neither never nevertheless new next nine no nobody non none noone nor normally not nothing novel now nowhere o obviously of off often oh ok okay old on once one ones only onto or other others otherwise ought our ours ourselves out outside over overall own p particular particularly per perhaps placed please plus possible presumably probably provides q que quite qv r rather rd re really reasonably regarding regardless regards relatively respectively right s said same saw say saying says second secondly see seeing seem seemed seeming seems seen self selves sensible sent serious seriously seven several shall she should shouldn\'t since six so some somebody somehow someone something sometime sometimes somewhat somewhere soon sorry specified specify specifying still sub such sup sure t t\'s take taken tell tends th than thank thanks thanx that that\'s thats the their theirs them themselves then thence there there\'s thereafter thereby therefore therein theres thereupon these they they\'d they\'ll they\'re they\'ve think third this thorough thoroughly those though three through throughout thru thus to together too took toward towards tried tries truly try trying twice two u un under unfortunately unless unlikely until unto up upon us use used useful uses using usually uucp v value various very via viz vs w want wants was wasn\'t way we we\'d we\'ll we\'re we\'ve welcome well went were weren\'t what what\'s whatever when whence whenever where where\'s whereafter whereas whereby wherein whereupon wherever whether which while whither who who\'s whoever whole whom whose why will willing wish with within without won\'t wonder would would wouldn\'t x y yes yet you you\'d you\'ll you\'re you\'ve your yours yourself yourselves z zero';

                var stopwords = {};
                _stopwords.split(' ').forEach(function(w) { stopwords[w] = true; });

                var word_segmenter = function(sent) {
                    return sent.split(/\b/)
                        .map(function(w) { return w.trim().toLowerCase(); })
                        .filter(function(w) {
                            return (stopwords[w] == null && w.length > 0 &&
                            ! /^[\s!-@\[-`\{-~　、-〜！-＠［-｀]+$/.test(w));
                        });
                };

                var sentences = sent_splitter(text);
                var result = lexrank(sentences, {word_segmenter: word_segmenter})
                    .slice(0, num_sent);
                //console.log(result);
                var score_max = result[0].score;
                var score_min = result.slice(-1)[0].score;
                result.sort(function(a, b) { return a.idx - b.idx; });

                var score2color = function(s) { // need review
                    // [0,1] -> [9,0] -> #nnnnnn
                    var c = '#';
                    var n = Math.floor((1 - s) * 9);
                    for (var i = 0; i < 6; i++) c += n;
                    return c;
                };

                var summary = '';
                result.forEach(function(meta) {
                    //console.log('(' + meta.score + ', ' + meta.idx + ')  ' + sentences[meta.idx]);
                    var score_norm = (meta.score - score_min) / (score_max - score_min);
                    //console.log(meta.score + ' ' + score_norm);
                    var color = score2color(score_norm);
                    meta.color = color;
                    //console.log(color);
                    summary += '<span style="color:' + color + '"> ' + sentences[meta.idx] + '</span>';
                });
                var response = '';
                result.map(function(r) {
                    r.text = sentences[r.idx];
                    response += sentences[r.idx];
                });
                cb({result: result, text: response});
            }
        };
	}
]);

/* jshint ignore:end */
