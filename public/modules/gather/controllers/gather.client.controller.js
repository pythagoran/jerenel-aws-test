'use strict';

angular.module('gather').controller('GatherController', ['$scope', 'Gather',
	function($scope, Gather) {

        $scope.fetch = {
            url: 'mashable.com',
            selector: 'article',
            extract: ''
        };

        $scope.gather = function() {
            $scope.gathering = Gather.get({
                url: 'http://' + $scope.fetch.url,
                selector: $scope.fetch.selector,
                extract: $scope.fetch.extract
            });
        };

        $scope.$watch('gathering', function(val) {
            console.log(val);
        });
	}
]);
