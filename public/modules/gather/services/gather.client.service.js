'use strict';

// Gather service used for communicating with the gather REST endpoints
angular.module('gather').factory('Gather', ['$resource',
    function($resource) {
        return $resource('/api/gather');
    }
]);
