'use strict';

// Configuring the Gather module
angular.module('gather').run(['Menus',
    function(Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Gatherer', 'gatherer', 'item', '/gatherer', false);
    }
]);
