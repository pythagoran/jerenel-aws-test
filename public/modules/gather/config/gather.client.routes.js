'use strict';

//Setting up route
angular.module('gather').config(['$stateProvider',
	function($stateProvider) {
		// Gather state routing
		$stateProvider.
		state('gather', {
			url: '/gatherer',
			templateUrl: 'modules/gather/views/gather.client.view.html'
		});
	}
]);
