'use strict';

(function() {
	// Readers Controller Spec
	describe('Readers Controller Tests', function() {
		// Initialize global variables
		var ReadersController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Readers controller.
			ReadersController = $controller('ReadersController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Reader object fetched from XHR', inject(function(Readers) {
			// Create sample Reader using the Readers service
			var sampleReader = new Readers({
				name: 'New Reader'
			});

			// Create a sample Readers array that includes the new Reader
			var sampleReaders = [sampleReader];

			// Set GET response
			$httpBackend.expectGET('readers').respond(sampleReaders);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.readers).toEqualData(sampleReaders);
		}));

		it('$scope.findOne() should create an array with one Reader object fetched from XHR using a readerId URL parameter', inject(function(Readers) {
			// Define a sample Reader object
			var sampleReader = new Readers({
				name: 'New Reader'
			});

			// Set the URL parameter
			$stateParams.readerId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/readers\/([0-9a-fA-F]{24})$/).respond(sampleReader);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.reader).toEqualData(sampleReader);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Readers) {
			// Create a sample Reader object
			var sampleReaderPostData = new Readers({
				name: 'New Reader'
			});

			// Create a sample Reader response
			var sampleReaderResponse = new Readers({
				_id: '525cf20451979dea2c000001',
				name: 'New Reader'
			});

			// Fixture mock form input values
			scope.name = 'New Reader';

			// Set POST response
			$httpBackend.expectPOST('readers', sampleReaderPostData).respond(sampleReaderResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Reader was created
			expect($location.path()).toBe('/readers/' + sampleReaderResponse._id);
		}));

		it('$scope.update() should update a valid Reader', inject(function(Readers) {
			// Define a sample Reader put data
			var sampleReaderPutData = new Readers({
				_id: '525cf20451979dea2c000001',
				name: 'New Reader'
			});

			// Mock Reader in scope
			scope.reader = sampleReaderPutData;

			// Set PUT response
			$httpBackend.expectPUT(/readers\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/readers/' + sampleReaderPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid readerId and remove the Reader from the scope', inject(function(Readers) {
			// Create new Reader object
			var sampleReader = new Readers({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Readers array and include the Reader
			scope.readers = [sampleReader];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/readers\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleReader);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.readers.length).toBe(0);
		}));
	});
}());