'use strict';

// Configuring the Readers module
angular.module('readers').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Reader', 'reader', 'item', '/reader', false);
	}
]);
