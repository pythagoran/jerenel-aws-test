'use strict';

//Setting up route
angular.module('readers').config(['$stateProvider',
	function($stateProvider) {
		// Readers state routing
		$stateProvider.
		state('reader', {
			url: '/reader',
			templateUrl: 'modules/readers/views/readers.client.view.html'
		});
	}
]);
