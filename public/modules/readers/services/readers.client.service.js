'use strict';

//Readers service used to communicate Readers REST endpoints
angular.module('readers').factory('Readers', ['$resource',
	function($resource) {
		return $resource('api/readers');
	}
]);
