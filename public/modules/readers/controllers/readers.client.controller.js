'use strict';

// Readers controller
angular.module('readers').controller('ReadersController', ['$scope', '$q', 'Authentication', 'Readers', 'ngAudio', 'RequestFactory',
	function($scope, $q, Authentication, Readers, ngAudio, RequestFactory) {
        $scope.authentication = Authentication;

        $scope.todos = [
            {val: 'Text-to-speech',
                more:[
                    'Split audio tracks in sentences',
                    'Stream audio playlist',
                    'Download single audio file'
                ]
            },
            {val: 'spreeder.com implementation (+features)',
                more: [
                    'Close-captioned reading',
                    'Flashing chunks',
                    'Scrolling text @ speed'
                ]
            }
        ];

        $scope.ttsText = {
            data:'Though Justin Trudeau said it as recently as last week, the Liberals have been claiming since at least 2013 that Canada\'s economic growth under the Harper-led Conservative government has been the worst since the 1930s.' + '\n' + 'Among those who use it often is Liberal MP — and former federal finance minister — Ralph Goodale.' + '\n' + 'In an April speech, Goodale added some detail, noting that "since the Harper government came to power, Canada has had an average annual economic growth rate of only about 1.75 per cent — that\'s the worst growth record of any prime minister since R. B. Bennett in the 1930s."' + '\n' + 'How accurate is the Liberal claim?'
        };

        $scope.read = function () {
            // create helper factory to manipulate text
            // using nlp tools + lodash
            console.log('speed reading');
        };

        $scope.speak = {
            config: {
                playbackRate: {
                    default: 1.5,
                    max: 3,
                    min: 1,
                    step: 0.25
                },
                progression: {
                    step: function() {
                        return 2.5 * $scope.audio.audio.playbackRate;
                    },
                    skip: function(loc) {
                        // jump to beginning / end of sentence
                        // if (loc===0) return start
                        // if (loc===1) return end
                        return 2.5;
                    }
                },
                volume: {
                    max: 1,
                    min: 0.1,
                    step: 0.1
                }
            },
            exec: function () {
                // attempt to implement RequestFactory to allow aborting requests
                // usage article:   https://developer.rackspace.com/blog/cancelling-ajax-requests-in-angularjs-applications/
                // git:             https://github.com/sathify/angular-abortable-requests
                Readers.get({data: $scope.ttsText.data}, function (data) {
                    // data returns [file, (exec)time]
                    $scope.audio = ngAudio.play(data.file);
                    $scope.audio.setPlaybackRate($scope.speak.config.playbackRate.default);
                });
            }
        };

        $scope.$watch('audio', function(val) { console.log(val); });

        $scope.$on('select', function(obj, evt) {
            // wrap in span and apply class
            // onhover menu with options:
            //   add to queue
            //   speak
            //   speed read
            console.log(evt);
        });

        $scope.$on('esc', function (obj, evt) {
            console.log('esc');
        });

        $scope.$on('up', function(obj, evt) {
            // increase playbackRate
            if (evt.ctrlKey || !(_.includes('input textarea', evt.target.localName)))
                if ($scope.audio.audio.playbackRate < $scope.speak.config.playbackRate.max)
                    $scope.audio.setPlaybackRate($scope.audio.audio.playbackRate += $scope.speak.config.playbackRate.step);
        });

        $scope.$on('down', function(obj, evt) {
            if (evt.ctrlKey || !(_.includes('input textarea', evt.target.localName)))
                if ($scope.audio.audio.playbackRate > $scope.speak.config.playbackRate.min)
                    $scope.audio.setPlaybackRate($scope.audio.audio.playbackRate -= $scope.speak.config.playbackRate.step);
        });

        $scope.$on('left', function(obj, evt) {
            var step = $scope.speak.config.progression.step();
            if (evt.shiftKey) step = $scope.speak.config.progression.skip(0);
            if (!(_.includes('input textarea', evt.target.localName)))
                if (step < $scope.audio.currentTime)
                    $scope.audio.setCurrentTime($scope.audio.currentTime - step);
        });

        $scope.$on('right', function(obj, evt) {
            var step = $scope.speak.config.progression.step();
            if (evt.shiftKey) step = $scope.speak.config.progression.skip(1);
            if (!(_.includes('input textarea', evt.target.localName)))
                if (step < $scope.audio.remaining)
                    $scope.audio.setCurrentTime($scope.audio.currentTime + step);
        });

        $scope.$on('space', function(obj, evt) {
            if (!(_.includes('input textarea', evt.target.localName)))
                if ($scope.audio.paused) $scope.audio.play();
                else $scope.audio.pause();
        });

        $scope.$on('equals', function(obj, evt) {
            if (!(_.includes('input textarea', evt.target.localName)))
                if ($scope.audio.volume < $scope.speak.config.volume.max)
                $scope.audio.volume += $scope.speak.config.volume.step;
        });

        $scope.$on('dash', function(obj, evt) {
            if (!(_.includes('input textarea', evt.target.localName)))
                if ($scope.audio.volume > $scope.speak.config.volume.min)
                $scope.audio.volume -= $scope.speak.config.volume.step;
        });

        $scope.$on('m', function(obj, evt) {
            if (!(_.includes('input textarea', evt.target.localName)))
                // Cache current volume!
                if ($scope.audio.volume === 0) $scope.audio.volume = 1;
                else $scope.audio.volume = 0;
        });

	}
]);
