'use strict';

//Setting up route
angular.module('sage').config(['$stateProvider',
	function($stateProvider) {
		// Sages state routing
		$stateProvider.
		state('sage', {
			url: '/sage',
			templateUrl: 'modules/sage/views/sage.client.view.html'
		});
	}
]);
