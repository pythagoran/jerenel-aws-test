'use strict';

// Configuring the Sage module
angular.module('sage').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Sage', 'sage', 'item', '/sage', false);
	}
]);
