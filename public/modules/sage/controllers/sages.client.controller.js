'use strict';

// Sage controller
angular.module('sage').controller('SageController', ['$scope', '$stateParams', '$location', 'Authentication', 'Sage',
	function($scope, $stateParams, $location, Authentication, Sage) {
		$scope.authentication = Authentication;

        $scope.todos = [
            {val: 'Database analysis/interpretation/manipulation', more: ['Search/sort/filter by author/tag/category', 'Range settings', 'Period comparison']},
            {val: 'Automated media-list creation'},
            {val: 'Automated news summaries/reports in category/author/range'},
            {val: 'Trend analysis', more: ['Mainstream news', 'Twitter trends/mentions']}
        ];

		// Find a list of Sages
		$scope.find = function() {
			$scope.sages = Sage.query();
		};
	}
]);
