'use strict';

//Sage service used to communicate Sage REST endpoints
angular.module('sage').factory('Sage', ['$resource',
	function($resource) {
		return $resource('api/sage');
	}
]);
