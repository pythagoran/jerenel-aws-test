'use strict';

angular.module('core').directive('eventControls', [
    function() {

        var keysByCode = {
            8: 'backspace',
            9: 'tab',
            13: 'enter',
            27: 'esc',
            32: 'space',
            33: 'pageup',
            34: 'pagedown',
            35: 'end',
            36: 'home',
            37: 'left',
            38: 'up',
            39: 'right',
            40: 'down',
            45: 'insert',
            46: 'delete',
            48: '0',
            49: '1',
            50: '2',
            51: '3',
            52: '4',
            53: '5',
            54: '6',
            55: '7',
            56: '8',
            57: '9',
            65: 'a',
            66: 'b',
            67: 'c',
            68: 'd',
            69: 'e',
            70: 'f',
            71: 'g',
            72: 'h',
            73: 'i',
            74: 'j',
            75: 'k',
            76: 'l',
            77: 'm',
            78: 'n',
            79: 'o',
            80: 'p',
            81: 'q',
            82: 'r',
            83: 's',
            84: 't',
            85: 'u',
            86: 'v',
            87: 'w',
            88: 'x',
            89: 'y',
            90: 'z',
            187: 'equals',
            188: 'comma',
            189: 'dash',
            190: 'period'
        };

        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                elem.on('keydown', function(evt) {
                    if (keysByCode[evt.which])
                        scope.$broadcast(keysByCode[evt.which], evt);
                    // see ui.keyPress implementation:
                    // https://github.com/angular-ui/ui-utils/tree/master/modules/keypress
                });
                elem.on('select', function(evt) {
                    scope.$broadcast('select', evt);
                });
            }
        };
    }
]);
