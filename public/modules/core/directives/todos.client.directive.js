'use strict';

angular.module('core').directive('todos', [
	function() {
		return {
			restrict: 'E',
            scope: '=',
            templateUrl: 'modules/core/views/todos.client.view.html'

        };
	}
]);
